let buttons = document.querySelectorAll('.btn'); 

document.addEventListener ('keydown', (event) => { 
  buttons.forEach((btn) => { 
    if (btn.classList.contains("highlighter")) { 
      btn.classList.remove("highlighter"); 
    } else if (`Key${btn.dataset.key}` == event.code || (
      btn.dataset.key == "Enter" && event.key == "Enter")) { 
        btn.classList.add("highlighter"); 
    } 
  }); 
})